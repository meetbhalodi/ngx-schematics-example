## Schematic for generating custom components
please refer to the original tutorial videos from Decoded Frontend.
1. [Video 1](https://youtu.be/MVqVBbM_gvw)
2. [Video 2](https://youtu.be/dADWO1Wh6-4)

Original Repository link -> https://github.com/DMezhenskyi/ngx-schematics-lib-example

## Setup for creating a library

1. create new workspace by the following command
`ng new ngx-schematics-example --no-create-application`
2. navigate to the above workspace and create new library using below command
`ng generate library super-ui-lib`
This will create a new library called super-ui-lib inside projects folder
3. Inside super-ui-lib create a new folder called `schematics` and inside this folder create a file `collection.json` and add following content.
```
{
    "$schema": "../../../node_modules/@angular-devkit/schematics/collection-schema.json",
    "schematics": {
      "ng-add": {
        "description": "Add Super UI Library to the project",
        "factory": "./ng-add/index#ngAdd"
      },
      "super-ui-component": {
        "description": "This schematic generates super component",
        "factory": "./super-ui-component/index#superUIComponentGenerator",
        "schema": "./super-ui-component/schema.json",
        "aliases": [
          "suc"
        ]
      }
    }
  }
```
4. Now create two folder named `ng-add` and `super-ui-component` inside `schematics` folder. This is basically our collections for which we are making the schematics.
5. Indside `ng-add` filder create `index.ts` file and add the following content
```
import { Rule, Tree, SchematicContext, SchematicsException } from '@angular-devkit/schematics';
import { NodePackageInstallTask } from "@angular-devkit/schematics/tasks";
import {applyToUpdateRecorder} from '@schematics/angular/utility/change';
import { addImportToModule } from '@schematics/angular/utility/ast-utils';
import * as ts from 'typescript';

export function ngAdd(): Rule {
  return (tree: Tree, context: SchematicContext) => {
    context.logger.info('Adding library Module to the app...');
    const modulePath = '/src/app/app.module.ts';
    if(!tree.exists(modulePath)) {
      throw new SchematicsException(`The file ${modulePath} doesn't exists...`);
    }
    const recorder = tree.beginUpdate(modulePath);

    const text = tree.read(modulePath);
    
    if(text === null) {
      throw new SchematicsException(`The file ${modulePath} doesn't exists...`);
    }

    const source = ts.createSourceFile(
      modulePath,
      text.toString(),
      ts.ScriptTarget.Latest,
      true
    );

    applyToUpdateRecorder(recorder, 
      addImportToModule(source, modulePath, 'SuperUiLibModule', 'super-ui-lib')  
    );

    tree.commitUpdate(recorder);


    context.logger.info('Installing dependencies...');
    context.addTask(new NodePackageInstallTask());
    return tree;
  }
}
```
6. Inside the `super-ui-component` folder create `index.ts`, `schema.json` and `super-conponent.ts` files.
7. In `index.ts` file add following content
```
import { apply, applyTemplates, chain, externalSchematic, MergeStrategy, mergeWith, move, Rule, url } from "@angular-devkit/schematics";
import { strings, normalize } from "@angular-devkit/core";
import { SuperUIComponentSchema } from "./super-component";

export function superUIComponentGenerator(options: SuperUIComponentSchema): Rule {
  return () => {
    const templateSource = apply(
      url('./files'), [
        applyTemplates({
          classify: strings.classify,
          dasherize: strings.dasherize,
          name: options.name
        }),
        move(normalize(`/${options.path}/${strings.dasherize(options.name)}`))
      ]
    )
    return chain([
      externalSchematic('@schematics/angular', 'component', options),
      mergeWith(templateSource, MergeStrategy.Overwrite)
    ])
  }
}
```
8. In `schema.json` add the following content
```
{
    "$schema": "http://json-schema.org/schema",
    "$id": "super-ui-component",
    "title": "Super UI library Component Schema",
    "type": "object",
    "properties": {
      "name": {
        "description": "The name of the component",
        "type": "string"
      },
      "path": {
        "description": "Where should be created the file",
        "type": "string",
        "format": "path",
        "x-prompt": "Where should be created the file?",
        "default": "src/app"
      }
    },
    "required": []
  }
```
9. In `super-conponent.ts` add the following content.
```
export interface SuperUIComponentSchema {
    name: string;
    path: string;
  }
```
9. Also create a folder named `files` inside `super-ui-component` folder, which will contain all the predefined template which we want to create.
10. Inside `files` folder create these following files
    1. `__name@dasherize__.component.html.template`
    2. `__name@dasherize__.component.pug.template`
    3. `__name@dasherize__.component.ts.template`
    4. `__name@dasherize__.component.scss.template`
11. In Pug template file please add the following code 
```
p <%= dasherize(name) %> is working
```
(or you can also add customized code as per your requirement, which is basically the purpose of making this schematic. but for now I will add some sample code).
12. We can also add some custom code in typescript template
```
// Custom component made by us

import { Component, OnInit } from '@angular/core';
import { SuperUiLibService } from 'super-ui-lib'

@Component({
  selector: '<%= dasherize(name) %>',
  template: `<p><%= dasherize(name) %> works!</p>`
})
export class <%= classify(name) %>Component implements OnInit {

  constructor(private superUIService: SuperUiLibService) { }

  ngOnInit(): void {
  }

}
```
13. We also need to update `package.json` of our library. please add the following content in the file.
```
{
  ....
  "scripts": {
    "build": "tsc -p tsconfig.schematics.json",
    "postbuild": "copyfiles schematics/collection.json schematics/*/schema.json schematics/*/files/** ../../dist/super-ui-lib/"
  },
  ....
  "publishConfig": {
    "registry": "https://gitlab.com/api/v4/projects/36688345/packages/npm/"
  },
  "schematics": "./schematics/collection.json",
  ....
}
```
14. We also need to create `tsconfig.schematics.json` file in out library folder and update the content given below.
```
{
    "compilerOptions": {
      "baseUrl": ".",
      "lib": [
        "es2018",
        "dom"
      ],
      "declaration": true,
      "module": "commonjs",
      "moduleResolution": "node",
      "noEmitOnError": true,
      "noFallthroughCasesInSwitch": true,
      "noImplicitAny": true,
      "noImplicitThis": true,
      "noUnusedParameters": true,
      "noUnusedLocals": true,
      "rootDir": "schematics",
      "outDir": "../../dist/super-ui-lib/schematics",
      "skipDefaultLibCheck": true,
      "skipLibCheck": true,
      "sourceMap": true,
      "strictNullChecks": true,
      "target": "es6",
      "types": [
        "jasmine",
        "node"
      ]
    },
    "include": [
      "schematics/**/*"
    ],
    "exclude": [
      "schematics/*/files/**/*"
    ]
  }
```
15. Now our schematic is ready and it is ready to publish to Gitlab.
16. So at the root of our workspace `ngx-schematics-example` run the following command
    1. `npm run build`
    2. `npm run build --prefix projects/super-ui-lib`

Above both commands will create a build in `dist/super-ui-lib` folder

18. Now before we publish you had noticed that we have configured a `publishConfig` in package.json file. so basically we have to add our registry id given in following exapmple.
`https://example.gitlab.com/api/v4/projects/<project-id>/packages/npm/`

Where project id will be found at the homepage of your repository website in Gitlab.

19. We will also need to setup authentication token to publish in this repo, which can be found in [Gitlab Docs](https://docs.gitlab.com/ee/user/packages/npm_registry/), so please also refer this.

20. After this process is done our build is ready to publish. So now navigate to the build by `cd dist/super-ui-lib` and run `npm publish`.

21. Now we can add this package in our project we just need to add the following command
```
ng add super-ui-lib --registry=https://example.gitlab.com/api/v4/projects/<project-id>/packages/npm/
```
here the url given is sample URL but in actual we will need to add the Proper url of the project.

22. After adding the package we can try creating the compone t by using this package.

23. run `ng g super-ui-lib:super-ui-component --name TestComp` so this will generate a component with the given name with our customized code that we have added to the template files.



